from django import forms
from django.db import connection
from django.http import request

class Create_Reservasi_Hotel(forms.Form):
    nik_list = []
    nik = forms.ChoiceField(choices=nik_list, required=True)
    tanggal_masuk = forms.DateField(label=(
        "Tanggal Masuk"), required=True, widget=forms.DateInput(attrs={'type': 'date', 'id': 'tanggal_masuk'}))
    tanggal_keluar = forms.DateField(label=(
        "Tanggal Keluar"), required=True, widget=forms.DateInput(attrs={'type': 'date', 'id': 'tanggal_keluar'}))
    kode_hotel_list = []
    kode_hotel = forms.ChoiceField(
        label=('Kode Hotel'), choices=kode_hotel_list, required=True, widget=forms.Select(attrs={'id': 'kode_hotel'}))

    def __init__(self, user, peran, *args, **kwargs):
        super(Create_Reservasi_Hotel, self).__init__(*args, **kwargs)
        self.user = user
        print("Usernameku,", self.user)
        with connection.cursor() as cursor:
            if peran == 'Admin Satgas':
                cursor.execute("SELECT NIK,NIK FROM SIRUCO.PASIEN")
                self.fields['nik'].choices = cursor.fetchall()
            else:
                cursor.execute("SELECT NIK,NIK FROM SIRUCO.PASIEN WHERE IDPENDAFTAR = %s", [self.user])
                self.fields['nik'].choices = cursor.fetchall()
            cursor.execute(
                "SELECT kode,kode FROM SIRUCO.hotel ORDER BY kode ASC")
            self.fields['kode_hotel'].choices = cursor.fetchall()
            pilih = self.fields['kode_hotel'].choices[0][0]

class Update_Reservasi_Hotel(forms.Form):
    nik = forms.CharField(
        label=("NIK Pasien"), required=True, max_length=3)
    tanggal_masuk = forms.DateField(label=(
        "Tanggal Masuk"), required=True, widget=forms.DateInput(attrs={'type': 'date', 'id': 'tanggal_masuk'}))
    tanggal_keluar = forms.DateField(label=(
        "Tanggal Keluar"), required=True, widget=forms.DateInput(attrs={'type': 'date', 'id': 'tanggal_keluar'}))
    kode_hotel = forms.CharField(
        label=("Kode RS"), required=True, max_length=30)
    kode_ruangan = forms.CharField(
        label=("Kode Ruangan"), required=True, max_length=30)

    def __init__(self, *args, **kwargs):
        super(Update_Reservasi_Hotel, self).__init__(*args, **kwargs)
        self.fields['nik'].widget.attrs['readonly'] = True
        self.fields['nik'].widget.attrs['class'] = 'disabled'
        self.fields['tanggal_masuk'].widget.attrs['readonly'] = True
        self.fields['tanggal_masuk'].widget.attrs['class'] = 'disabled'
        self.fields['kode_hotel'].widget.attrs['readonly'] = True
        self.fields['kode_hotel'].widget.attrs['class'] = 'disabled'
        self.fields['kode_ruangan'].widget.attrs['readonly'] = True
        self.fields['kode_ruangan'].widget.attrs['class'] = 'disabled'
