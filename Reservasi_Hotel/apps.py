from django.apps import AppConfig


class ReservasiHotelConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Reservasi_Hotel'
