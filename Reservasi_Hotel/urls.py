from django.urls import path
from .views import change_ruangan_hotel, create_reservasi_hotel, read_reservasi_hotel, update_reservasi_hotel, delete_reservasi_hotel
app_name = "Reservasi_Hotel"

urlpatterns = [
    path('create/', create_reservasi_hotel, name='create'),
    path('read/', read_reservasi_hotel, name='read'),
    path('ajax/', change_ruangan_hotel, name='change_ruangan_hotel'),
    path('update/', update_reservasi_hotel, name='update'),
    path('delete/', delete_reservasi_hotel, name='delete'),
]
