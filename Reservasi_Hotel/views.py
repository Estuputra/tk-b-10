from django.shortcuts import render
from django.core import serializers
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *
from django.db import connection
from datetime import datetime

def user_login_required(function):
    def wrapper(request, *args, **kwargs):
        if 'is_login' not in request.session:
            return redirect('login:login_or_register')
        else:
            return function(request, *args, **kwargs)
    return wrapper

@ user_login_required
def create_reservasi_hotel(request):
    if request.session['peran'] != 'Admin Satgas' and request.session['peran'] != 'Pengguna Publik':
        return redirect('login:login_or_register')
    form = Create_Reservasi_Hotel(request.session['username'], request.session['peran'])
    if request.method == "POST":
        kode_pasien = request.POST['nik']
        tgl_masuk = request.POST['tanggal_masuk']
        tgl_keluar = request.POST['tanggal_keluar']
        kode_hotel = request.POST['kode_hotel']
        kode_room = request.POST['kode_ruangan']

        data_baru = [kode_pasien, tgl_masuk, tgl_keluar, kode_hotel, kode_room]
        print(data_baru)
        with connection.cursor() as cursor:
            cursor.execute(
                'INSERT INTO SIRUCO.RESERVASI_HOTEL VALUES (%s, %s, %s, %s, %s)', data_baru)
        return redirect('Reservasi_Hotel:read')
    return render(request, 'Reservasi_Hotel/create_reservasi_hotel.html', {'form': form})

def change_ruangan_hotel(request):
    print(request.GET.get('kode_hotel'))
    kode_hotel = request.GET.get('kode_hotel')
    print(kode_hotel)
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT KODEROOM FROM SIRUCO.HOTEL_ROOM WHERE KODEHOTEL = %s", [kode_hotel])
        data = cursor.fetchall()
    list_ruangan = []
    for i in data:
        list_ruangan.append(i[0])

    return JsonResponse({'ruangan': list_ruangan})

@user_login_required
def read_reservasi_hotel(request):
    if request.session['peran'] != 'Pengguna Publik' and request.session['peran'] != 'Admin Satgas':
        return redirect('login:login_or_register')
    if request.session['peran'] == 'Pengguna Publik':
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT * FROM SIRUCO.RESERVASI_HOTEL WHERE KODEPASIEN IN (SELECT NIK FROM SIRUCO.PASIEN WHERE idpendaftar= %s)", [request.session['username']])
            data = dictfetchall(cursor)
        context = {'apt': data, 'date': datetime.today().strftime('%Y-%m-%d')}
    else:
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.RESERVASI_HOTEL")
            data = dictfetchall(cursor)
        context = {'apt': data, 'date': datetime.today().strftime('%Y-%m-%d')}
    return render(request, 'Reservasi_Hotel/read_reservasi_hotel.html', context)

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

@user_login_required
def update_reservasi_hotel(request):
    if request.session['peran'] != 'Admin Satgas':
        return redirect('login:login_or_register')
    form = Update_Reservasi_Hotel()
    if request.method == 'GET':
        print(request.GET.get('kodepasien'), request.GET.get('tglmasuk'))
        if request.GET.get('kodepasien') is not None and request.GET.get('tglmasuk') is not None:
            print(23232)
            kodepasien = request.GET.get('kodepasien')
            tglmasuk = request.GET.get('tglmasuk')
            with connection.cursor() as cursor:
                cursor.execute(
                    'SELECT * FROM SIRUCO.RESERVASI_HOTEL where kodepasien = %s AND tglmasuk = %s LIMIT 1;', [kodepasien, tglmasuk])
                data = dictfetchall(cursor)
            data_pasien = {}
            data_pasien['nik'] = data[0]['kodepasien']
            data_pasien['tanggal_masuk'] = data[0]['tglmasuk']
            data_pasien['tanggal_keluar'] = data[0]['tglkeluar']
            data_pasien['kode_hotel'] = data[0]['kodehotel']
            data_pasien['kode_ruangan'] = data[0]['koderoom']
            form = Update_Reservasi_Hotel(initial=data_pasien)
    else:
        kodepasien = request.POST['nik']
        tglkeluar = request.POST['tanggal_keluar']
        tglmasuk = request.POST['tanggal_masuk']
        data_to_update = [tglkeluar, kodepasien, tglmasuk]
        with connection.cursor() as cursor:
            cursor.execute(
                "UPDATE SIRUCO.RESERVASI_HOTEL SET tglkeluar = %s WHERE KODEPASIEN = %s and TGLMASUK = %s", data_to_update)
        return redirect('Reservasi_Hotel:read')
    return render(request, 'Reservasi_Hotel/update_reservasi_hotel.html', {'form': form})


@user_login_required
def delete_reservasi_hotel(request):
    if request.session['peran'] != 'Admin Satgas':
        return redirect('login:login_or_register')
    if request.method == 'GET':
        if request.GET.get('kodepasien') is not None and request.GET.get('tglmasuk') is not None:
            kodepasien = request.GET.get('kodepasien')
            tglmasuk = request.GET.get('tglmasuk')
            with connection.cursor() as cursor:
                cursor.execute(
                    'SELECT idtransaksibooking FROM SIRUCO.TRANSAKSI_BOOKING WHERE kodepasien = %s AND tglmasuk = %s', [kodepasien, tglmasuk])
                data = dictfetchall(cursor)
                idtransaksi = data[0]['idtransaksibooking']
                cursor.execute(
                    'DELETE FROM SIRUCO.RESERVASI_HOTEL WHERE kodepasien = %s AND tglmasuk = %s', [kodepasien, tglmasuk])
                cursor.execute(
                    'DELETE FROM SIRUCO.TRANSAKSI_HOTEL WHERE idtransaksi = %s', [idtransaksi])
    return HttpResponseRedirect(reverse('Reservasi_Hotel:read'))