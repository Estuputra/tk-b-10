from django.shortcuts import render

# Create your views here.
from django.shortcuts import render

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *
from django.db import connection

#
# Create your views here.


def user_login_required(function):
    def wrapper(request, *args, **kwargs):
        if 'is_login' not in request.session:
            return redirect('login:login_or_register')
        else:
            return function(request, *args, **kwargs)
    return wrapper


@user_login_required
def create_RS(request):
    # Jika bukan pengguna publik
    if request.session['peran'] != 'Admin Satgas':
        return redirect('login:login_or_register')
    form = CreateRumahSakit()
    if request.method == "POST":
        kode_faskes = request.POST['faskes']
        isRujukan = ''
        try:
            if request.POST['isRujukan'] == '1':
                isRujukan = '1'
            else:
                isRujukan = '0'
        except:
            isRujukan = '0'

        data_baru = [kode_faskes, isRujukan]
        with connection.cursor() as cursor:
            cursor.execute(
                'INSERT INTO SIRUCO.RUMAH_SAKIT VALUES (%s, %s)', data_baru)
        return redirect('login:login_or_register')
    return render(request, 'rumahsakit/createRumahSakit.html', {'form': form, 'peran': request.session['peran']})


@user_login_required
def readRS(request):
    if request.session['peran'] != 'Admin Satgas':
        return redirect('login:login_or_register')
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.RUMAH_SAKIT")
        seluruh = dictfetchall(cursor)
    context = {'apt': seluruh}
    return render(request, 'rumahsakit/readRumahSakit.html', context)


@user_login_required
def updateRS(request):
    # Jika bukan pengguna publik
    if request.session['peran'] != 'Admin Satgas':
        return redirect('login:login_or_register')
    isRujukan = ''
    if request.method == 'GET':
        if request.GET.get('kode') is not None:
            kode = request.GET.get('kode')
            with connection.cursor() as cursor:
                cursor.execute(
                    'SELECT * FROM SIRUCO.RUMAH_SAKIT where kode_faskes=%s LIMIT 1;', [kode])
                data = dictfetchall(cursor)
            print(data)
            data_faskes = {}
            data_faskes['faskes'] = data[0]['kode_faskes']
            isRujukan = data[0]['isrujukan']
            form = UpdateRumahSakit(initial=data_faskes)
    else:
        # print('WOIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII')
        # print(request.POST)
        kode = request.POST['faskes']
        tipe = ''
        try:
            if request.POST['isRujukan'] == '1':
                tipe = '1'
            else:
                tipe = '0'
        except:
            tipe = '0'
        data_baru = [tipe, kode]
        with connection.cursor() as cursor:
            cursor.execute(
                "UPDATE SIRUCO.RUMAH_SAKIT SET isrujukan = %s WHERE kode_faskes = %s", data_baru)
        return redirect('rumahsakit:read')
    return render(request, 'rumahsakit/updateRumahSakit.html', {'form': form, 'isRujukan': isRujukan})


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
