from django.urls import path

# from .views import login, logout_view, login_or_register, register, register_admin_sistem, register_pengguna_publik, register_dokter, register_admin_satgas
from .views import create_RS, readRS, updateRS
app_name = 'rumahsakit'

urlpatterns = [
    path('create/', create_RS, name='create'),
    path('read/', readRS, name='read'),
    path('update/', updateRS, name='update'),
]
