from django.shortcuts import render
from django.core import serializers
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *
from django.db import connection

# Create your views here.

def user_login_required(function):
    def wrapper(request, *args, **kwargs):
        if 'is_login' not in request.session:
            return redirect('login:login_or_register')
        else:
            return function(request, *args, **kwargs)
    return wrapper

@ user_login_required
def create_paket_makan(request):
    # Jika bukan admin sistem
    if request.session['peran'] != 'Admin Sistem':
        return redirect('login:login_or_register')
    form = CreatePaketMakanForm()
    if request.method == "POST":
        kodehotel = request.POST['kodehotel']
        kodepaket = request.POST['kodepaket']
        nama = request.POST['nama']
        harga = request.POST['harga']

        new_data = [kodehotel, kodepaket,nama, harga]
        with connection.cursor() as cursor:
            cursor.execute(
                'INSERT INTO SIRUCO.PAKET_MAKAN VALUES (%s, %s, %s, %s)', new_data)
        return redirect('paketmakan:read')
    return render(request, 'CreatePaketMakan.html', {'form': form})

@user_login_required
def update_paket_makan(request):
    # Jika bukan admin sistem
    if request.session['peran'] != 'Admin Sistem':
        return redirect('login:login_or_register')

    #if request.method == 'GET':
    if request.GET.get('kodehotel') is not None:
        kodehotel = request.GET.get('kodehotel')
        with connection.cursor() as cursor:
            cursor.execute(
                'SELECT * FROM SIRUCO.PAKET_MAKAN where kodehotel=%s LIMIT 1;', [kodehotel])
            data = dictfetchall(cursor)
        data_pm = {}
        data_pm['kodehotel'] = data[0]['kodehotel']
        data_pm['kodepaket'] = data[0]['kodepaket']
        data_pm['nama'] = data[0]['nama']
        data_pm['harga'] = data[0]['harga']
        form = UpdatePaketMakanForm(initial=data_pm)
        
    if request.method == 'POST':
        kodehotel = request.POST['kodehotel']
        kodepaket = request.POST['kodepaket']
        nama = request.POST['nama']
        harga = request.POST['harga']
        new_data = [nama, harga, kodehotel, kodepaket]
        with connection.cursor() as cursor:
            cursor.execute(
                "UPDATE SIRUCO.PAKET_MAKAN SET nama = %s, harga = %s WHERE KODEHOTEL = %s and KODEPAKET = %s ", new_data)
        return redirect('paketmakan:read')
    form = UpdatePaketMakanForm()
    return render(request, 'UpdatePaketMakan.html', {'form': form})

@user_login_required
def delete_paket_makan(request):
    # Jika bukan admin sistem
    if request.session['peran'] != 'Admin Sistem':
        return redirect('login:login_or_register')
    if request.method == 'GET':
        if request.GET.get('kodehotel') is not None and request.GET.get('kodepaket') is not None:
            kodehotel = request.GET.get('kodehotel')
            kodepaket = request.GET.get('kodepaket')
            with connection.cursor() as cursor:
                cursor.execute(
                    'DELETE FROM SIRUCO.PAKET_MAKAN WHERE kodehotel = %s AND kodepaket = %s', [kodehotel, kodepaket])
    return HttpResponseRedirect(reverse('paketmakan:read'))

@user_login_required
def read_PaketMakan(request):
    # Jika bukan selain admin siste, admin satgas, dan pengguna publik
    if request.session['peran'] == 'Dokter':
        return redirect('login:login_or_register')
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.PAKET_MAKAN")
        rekap = dictfetchall(cursor)
    context = {'apt': rekap}
    return render(request, 'ReadPaketMakan.html', context)

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]