from django.urls import path

# from .views import login, logout_view, login_or_register, register, register_admin_sistem, register_pengguna_publik, register_dokter, register_admin_satgas
from .views import create_paket_makan, read_PaketMakan, update_paket_makan, delete_paket_makan

app_name = 'paketmakan'

urlpatterns = [
    path('create/', create_paket_makan, name='create'),
    path('read/', read_PaketMakan, name='read'),
    path('update/', update_paket_makan, name='update'),
    path('delete/', delete_paket_makan, name='delete'),
]
