from django import forms
from django.db import connection


class CreatePaketMakanForm(forms.Form):
    kodehotellist = []
    kodehotel = forms.ChoiceField(label=('Kode Hotel'),choices=kodehotellist, required=True, widget=forms.Select(attrs={'id': 'kode_hotel'}))
    kodepaket = forms.CharField(label=("Kode Paket"), required=True, max_length=5)
    nama = forms.CharField(label=("Nama Paket"), required=True, max_length=30)
    harga = forms.CharField(label=("Harga"), required=True, max_length=20)

    def __init__(self, *args, **kwargs):
        super(CreatePaketMakanForm, self).__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT kode,kode FROM SIRUCO.HOTEL ORDER BY KODE ASC")
            self.fields['kodehotel'].choices = cursor.fetchall()

class UpdatePaketMakanForm(forms.Form):
    kodehotel = forms.CharField(label=("Kode Hotel"), required=True, max_length=3)
    kodepaket = forms.CharField(label=("Kode Paket"), required=True, max_length=5)
    nama = forms.CharField(label=("Nama Paket"), required=True, max_length=30)
    harga = forms.CharField(label=("Harga"), required=True, max_length=20)

    def __init__(self, *args, **kwargs):
        super(UpdatePaketMakanForm, self).__init__(*args, **kwargs)
        self.fields['kodehotel'].widget.attrs['readonly'] = True
        self.fields['kodehotel'].widget.attrs['class'] = 'disabled'
        self.fields['kodepaket'].widget.attrs['readonly'] = True
        self.fields['kodepaket'].widget.attrs['class'] = 'disabled'