from django.apps import AppConfig


class PaketmakanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'paketmakan'
