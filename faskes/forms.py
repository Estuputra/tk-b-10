from django import forms
from django.db import connection


class CreateFaskesForm(forms.Form):
    tipe_faskes_list = [('Rumah Sakit', 'Rumah Sakit'),
                        ('Puskesmas', 'Puskesmas'), ('Klinik', 'Klinik')]
    status_faskes_list = [('Swasta', 'Swasta'), ('Pemerintah', 'Pemerintah')]
    kode = forms.CharField(label=("Kode Faskes"), required=True, max_length=3)
    tipe = forms.ChoiceField(
        label=('Tipe'), required=True, choices=tipe_faskes_list)
    nama = forms.CharField(label=("Nama Faskes"), required=True, max_length=50)
    status = forms.ChoiceField(
        label=('Status Kepemilikan'), required=True, choices=status_faskes_list)
    jalan = forms.CharField(label=("Jalan"), required=True, max_length=30)
    kelurahan = forms.CharField(
        label=("Kelurahan"), required=True, max_length=30)
    kecamatan = forms.CharField(
        label=("Kecamatan"), required=True, max_length=30)
    kabkot = forms.CharField(label=("Kabpuaten/Kota"),
                             required=True, max_length=30)
    provinsi = forms.CharField(
        label=("Provinsi"), required=True, max_length=30)

    def __init__(self, *args, **kwargs):
        super(CreateFaskesForm, self).__init__(*args, **kwargs)
        self.fields['kode'].widget.attrs['readonly'] = True
        self.fields['kode'].widget.attrs['class'] = 'disabled'
        kode_baru = ''
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT KODE FROM SIRUCO.FASKES ORDER BY KODE DESC LIMIT 1")
            # print(cursor.fetchall()[0][0][1:])
            kode_baru = 'F' + self.manip(cursor.fetchall()[0][0][1:])
        self.fields['kode'].initial = kode_baru

    def manip(self, kode):
        res = ''
        print(kode)
        if kode[-1] == '9':
            res += str(int(kode[0])+1) + '0'
        else:
            print(kode, 'OI')
            res += (kode[0] + str(int(kode[-1]) + 1))
        return res


class UpdateFaskesForm(forms.Form):
    tipe_faskes_list = [('Rumah Sakit', 'Rumah Sakit'),
                        ('Puskesmas', 'Puskesmas'), ('Klinik', 'Klinik')]
    status_faskes_list = [('Swasta', 'Swasta'), ('Pemerintah', 'Pemerintah')]
    kode = forms.CharField(label=("Kode Faskes"), required=True, max_length=3)
    tipe = forms.ChoiceField(
        label=('Tipe'), required=True, choices=tipe_faskes_list)
    nama = forms.CharField(label=("Nama Faskes"), required=True, max_length=50)
    status = forms.ChoiceField(
        label=('Status Kepemilikan'), required=True, choices=status_faskes_list)
    jalan = forms.CharField(label=("Jalan"), required=True, max_length=30)
    kelurahan = forms.CharField(
        label=("Kelurahan"), required=True, max_length=30)
    kecamatan = forms.CharField(
        label=("Kecamatan"), required=True, max_length=30)
    kabkot = forms.CharField(label=("Kabpuaten/Kota"),
                             required=True, max_length=30)
    provinsi = forms.CharField(
        label=("Provinsi"), required=True, max_length=30)

    def __init__(self, *args, **kwargs):
        super(UpdateFaskesForm, self).__init__(*args, **kwargs)
        self.fields['kode'].widget.attrs['readonly'] = True
        self.fields['kode'].widget.attrs['class'] = 'disabled'
