from django.apps import AppConfig


class TransaksiBookingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'transaksi_booking'
