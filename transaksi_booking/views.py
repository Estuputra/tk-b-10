from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.db import connection

def user_login_required(function):
    def wrapper(request, *args, **kwargs):
        if 'is_login' not in request.session:
            return redirect('login:login_or_register')
        else:
            return function(request, *args, **kwargs)
    return wrapper

@user_login_required
def read_transaksi_booking(request):
    if request.session['peran'] != 'Admin Satgas' and request.session['peran'] != 'Pengguna Publik':
        return redirect('login:login_or_register')
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.TRANSAKSI_BOOKING")
        data = dictfetchall(cursor)
    context = {'apt': data}
    return render(request, 'transaksi_booking/read_transaksi_booking.html', context)


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
