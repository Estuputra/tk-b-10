from django.urls import path
from .views import read_transaksi_booking
app_name = 'transaksi_booking'

urlpatterns = [
    path('read/', read_transaksi_booking, name='read'),
]