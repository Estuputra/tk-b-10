from django.urls import path

# from .views import login, logout_view, login_or_register, register, register_admin_sistem, register_pengguna_publik, register_dokter, register_admin_satgas
from .views import create_pasien, read_pasien, detail_pasien, update_pasien, delete_pasien
app_name = 'pasien'

urlpatterns = [
    path('create/', create_pasien, name='create'),
    path('read/', read_pasien, name='read'),
    path('details/', detail_pasien, name='details'),
    path('update/', update_pasien, name='update'),
    path('delete/', delete_pasien, name='delete'),
]
