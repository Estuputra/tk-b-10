from django import forms
from django.db import connection


class CreatePasienForm(forms.Form):
    pendaftar = forms.CharField(
        label=("Pendaftar"), required=True, max_length=50)
    NIK = forms.CharField(
        label=("NIK"), required=True, max_length=20)
    nama = forms.CharField(
        label=("Nama"), required=True, max_length=20)
    nomor_telepon = forms.CharField(
        label=("Nomor Telepon"), required=True, max_length=20)
    nomor_hp = forms.CharField(
        label=("Nomor HP"), required=True, max_length=20)
    jalan_ktp = forms.CharField(
        label=("Jalan (Sesuai KTP)"), required=True, max_length=20)
    kelurahan_ktp = forms.CharField(
        label=("Kelurahan (Sesuai KTP)"), required=True, max_length=20)
    kecamatan_ktp = forms.CharField(
        label=("Kecamatan (Sesuai KTP)"), required=True, max_length=20)
    kabupaten_ktp = forms.CharField(
        label=("Kabupaten / Kota (Sesuai KTP)"), required=True, max_length=20)
    provinsi_ktp = forms.CharField(
        label=("Provinsi (Sesuai KTP)"), required=True, max_length=20)

    jalan_domisili = forms.CharField(
        label=("Jalan (Sesuai Domisili)"), required=True, max_length=20)
    kelurahan_domisili = forms.CharField(
        label=("Kelurahan (Sesuai Domisili)"), required=True, max_length=20)
    kecamatan_domisili = forms.CharField(
        label=("Kabupaten / Kota (Sesuai Domisili)"), required=True, max_length=20)
    kabupaten_domisili = forms.CharField(
        label=("Kabupaten (Sesuai Domisili)"), required=True, max_length=20)
    provinsi_domisili = forms.CharField(
        label=("Provinsi (Sesuai Domisili)"), required=True, max_length=20)

    def __init__(self, *args, **kwargs):
        print(kwargs, "HALO")
        username = ''
        if(kwargs != {}):
            username = kwargs.pop('username')
        super(CreatePasienForm, self).__init__(*args, **kwargs)
        self.fields['pendaftar'].disabled = True
        # print(username)
        self.fields['pendaftar'].initial = username


class UpdatePasienForm(forms.Form):
    pendaftar = forms.CharField(
        label=("Pendaftar"), required=True, max_length=50)
    NIK = forms.CharField(
        label=("NIK"), required=True, max_length=20)
    nama = forms.CharField(
        label=("Nama"), required=True, max_length=20)
    nomor_telepon = forms.CharField(
        label=("Nomor Telepon"), required=True, max_length=20)
    nomor_hp = forms.CharField(
        label=("Nomor HP"), required=True, max_length=20)
    jalan_ktp = forms.CharField(
        label=("Jalan (Sesuai KTP)"), required=True, max_length=20)
    kelurahan_ktp = forms.CharField(
        label=("Kelurahan (Sesuai KTP)"), required=True, max_length=20)
    kecamatan_ktp = forms.CharField(
        label=("Kecamatan (Sesuai KTP)"), required=True, max_length=20)
    kabupaten_ktp = forms.CharField(
        label=("Kabupaten / Kota (Sesuai KTP)"), required=True, max_length=20)
    provinsi_ktp = forms.CharField(
        label=("Provinsi (Sesuai KTP)"), required=True, max_length=20)

    jalan_domisili = forms.CharField(
        label=("Jalan (Sesuai Domisili)"), required=True, max_length=20)
    kelurahan_domisili = forms.CharField(
        label=("Kelurahan (Sesuai Domisili)"), required=True, max_length=20)
    kecamatan_domisili = forms.CharField(
        label=("Kabupaten / Kota (Sesuai Domisili)"), required=True, max_length=20)
    kabupaten_domisili = forms.CharField(
        label=("Kabupaten (Sesuai Domisili)"), required=True, max_length=20)
    provinsi_domisili = forms.CharField(
        label=("Provinsi (Sesuai Domisili)"), required=True, max_length=20)

    def __init__(self, *args, **kwargs):
        super(UpdatePasienForm, self).__init__(*args, **kwargs)
        self.fields['pendaftar'].disabled = True
        self.fields['NIK'].widget.attrs['readonly'] = True
        self.fields['NIK'].widget.attrs['class'] = 'disabled'
        self.fields['nama'].disabled = True

        # print(username)
