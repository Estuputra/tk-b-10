from django.shortcuts import render

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *
from django.db import connection


def user_login_required(function):
    def wrapper(request, *args, **kwargs):
        if 'is_login' not in request.session:
            return redirect('login:login_or_register')
        else:
            return function(request, *args, **kwargs)
    return wrapper


@user_login_required
def create_pasien(request):
    # Jika bukan pengguna publik
    if request.session['peran'] != 'Pengguna Publik':
        return redirect('login:login_or_register')
    print(request.session['username'])
    form = CreatePasienForm(username=request.session['username'])
    if request.method == "POST":
        print('sss')
        print(1)
        # pendaftar = form.cleaned_data['pendaftar']
        nik = request.POST['NIK']
        nama = request.POST['nama']
        nomor_telepon = request.POST['nomor_telepon']
        nomor_hp = request.POST['nomor_hp']
        jalan_ktp = request.POST['jalan_ktp']
        kelurahan_ktp = request.POST['kelurahan_ktp']
        kecamatan_ktp = request.POST['kecamatan_ktp']
        kabupaten_ktp = request.POST['kabupaten_ktp']
        provinsi_ktp = request.POST['provinsi_ktp']
        jalan_domisili = request.POST['jalan_domisili']
        kelurahan_domisili = request.POST['kelurahan_domisili']
        kecamatan_domisili = request.POST['kecamatan_domisili']
        kabupaten_domisili = request.POST['kabupaten_domisili']
        provinsi_domisili = request.POST['provinsi_domisili']
        data_baru = [nik, request.session['username'], nama, jalan_ktp, kelurahan_ktp, kecamatan_ktp, kabupaten_ktp, provinsi_ktp,
                     jalan_domisili, kelurahan_domisili, kecamatan_domisili, kabupaten_domisili, provinsi_domisili, nomor_telepon, nomor_hp]
        with connection.cursor() as cursor:
            cursor.execute(
                'INSERT INTO SIRUCO.PASIEN VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s)', data_baru)
        return redirect('pasien:read')
    return render(request, 'pasien/createPasien.html', {'form': form, 'peran': request.session['peran']})


@user_login_required
def update_pasien(request):
    # Jika bukan pengguna publik
    if request.session['peran'] != 'Pengguna Publik':
        return redirect('login:login_or_register')
    if request.method == 'GET':
        if request.GET.get('nik') is not None:
            nik = request.GET.get('nik')
            with connection.cursor() as cursor:
                cursor.execute(
                    'SELECT * FROM SIRUCO.PASIEN where NIK=%s LIMIT 1;', [nik])
                data = dictfetchall(cursor)
            data_pasien = {}
            data_pasien['pendaftar'] = data[0]['idpendaftar']
            data_pasien['NIK'] = data[0]['nik']
            data_pasien['nama'] = data[0]['nama']
            data_pasien['nomor_telepon'] = data[0]['notelp']
            data_pasien['nomor_hp'] = data[0]['nohp']
            data_pasien['jalan_ktp'] = data[0]['ktp_jalan']
            data_pasien['kelurahan_ktp'] = data[0]['ktp_kelurahan']
            data_pasien['kecamatan_ktp'] = data[0]['ktp_kecamatan']
            data_pasien['kabupaten_ktp'] = data[0]['ktp_kabkot']
            data_pasien['provinsi_ktp'] = data[0]['ktp_prov']
            data_pasien['jalan_domisili'] = data[0]['dom_jalan']
            data_pasien['kelurahan_domisili'] = data[0]['dom_kelurahan']
            data_pasien['kecamatan_domisili'] = data[0]['dom_kecamatan']
            data_pasien['kabupaten_domisili'] = data[0]['dom_kabkot']
            data_pasien['provinsi_domisili'] = data[0]['dom_prov']
            form = UpdatePasienForm(initial=data_pasien)
    else:
        print(request.POST)
        nik = request.POST['NIK']
        nomor_telepon = request.POST['nomor_telepon']
        nomor_hp = request.POST['nomor_hp']
        jalan_ktp = request.POST['jalan_ktp']
        kelurahan_ktp = request.POST['kelurahan_ktp']
        kecamatan_ktp = request.POST['kecamatan_ktp']
        kabupaten_ktp = request.POST['kabupaten_ktp']
        provinsi_ktp = request.POST['provinsi_ktp']
        jalan_domisili = request.POST['jalan_domisili']
        kelurahan_domisili = request.POST['kelurahan_domisili']
        kecamatan_domisili = request.POST['kecamatan_domisili']
        kabupaten_domisili = request.POST['kabupaten_domisili']
        provinsi_domisili = request.POST['provinsi_domisili']
        data_to_update = [nomor_telepon, nomor_hp, jalan_ktp, kelurahan_ktp, kecamatan_ktp, kabupaten_ktp,
                          provinsi_ktp, jalan_domisili, kelurahan_domisili, kecamatan_domisili, kabupaten_domisili, provinsi_domisili, nik]
        with connection.cursor() as cursor:
            cursor.execute("UPDATE SIRUCO.PASIEN SET notelp = %s, nohp = %s,ktp_jalan = %s,ktp_kelurahan = %s,ktp_kecamatan = %s,ktp_kabkot = %s,ktp_prov = %s,dom_jalan = %s,dom_kelurahan = %s,dom_kecamatan = %s,dom_kabkot = %s,dom_prov = %s WHERE nik =%s", data_to_update)
        return redirect('pasien:read')
    return render(request, 'pasien/updatePasien.html', {'form': form})


@user_login_required
def delete_pasien(request):
    # Jika bukan pengguna publik
    if request.session['peran'] != 'Pengguna Publik':
        return redirect('login:login_or_register')
    if request.method == 'GET':
        if request.GET.get('nik') is not None:
            nik = request.GET.get('nik')
            with connection.cursor() as cursor:
                cursor.execute(
                    'DELETE FROM SIRUCO.PASIEN WHERE nik = %s', [nik])
    return HttpResponseRedirect(reverse('pasien:read'))


@user_login_required
def read_pasien(request):
    # Jika bukan pengguna publik
    if request.session['peran'] != 'Pengguna Publik':
        return redirect('login:login_or_register')
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.PASIEN WHERE IDPENDAFTAR = %s",
                       [request.session['username']])
        seluruh = dictfetchall(cursor)
    context = {'apt': seluruh}
    return render(request, 'pasien/readPasien.html', context)


@user_login_required
def detail_pasien(request):
    if request.session['peran'] != 'Pengguna Publik':
        return redirect('login:login_or_register')
    if request.method == 'GET':
        if request.GET.get('nik') is not None:
            nik = request.GET.get('nik')
            with connection.cursor() as cursor:
                cursor.execute(
                    'SELECT * FROM SIRUCO.PASIEN where NIK=%s LIMIT 1;', [nik])
                data = dictfetchall(cursor)

    return render(request, 'pasien/detailsPasien.html', data[0])


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
