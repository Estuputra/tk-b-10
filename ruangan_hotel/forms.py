from django import forms
from django.db import connection

class Create_Ruangan_Hotel_Form(forms.Form):
    kode_hotel_list = []
    kode_hotel = forms.ChoiceField(choices=kode_hotel_list, required=True)
    kode_ruangan = forms.CharField(label=("Kode Ruangan"), required=True, max_length=5)
    jenis_bed = forms.CharField(label=("Jenis Bed"), required=True, max_length=10)
    tipe = forms.CharField(label=("Tipe"), required=True, max_length=10)
    harga_per_hari = forms.IntegerField(label=("Harga per Hari"), required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['kode_hotel'].widget.attrs['id'] = 'kodehotel'
        self.fields['jenis_bed'].widget.attrs['id'] = 'jenisbed'
        self.fields['tipe'].widget.attrs['id'] = 'tipe'
        self.fields['harga_per_hari'].widget.attrs['id'] = 'harga'
        self.fields['kode_ruangan'].widget.attrs['readonly'] = True
        self.fields['kode_ruangan'].widget.attrs['class'] = 'disabled'
        self.fields['kode_ruangan'].widget.attrs['id'] = 'koderuangan'

        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT KODE,KODE FROM SIRUCO.HOTEL;")
            self.fields['kode_hotel'].choices = cursor.fetchall()

class Update_Ruangan_Hotel_Form(forms.Form):
    kode_hotel = forms.CharField(label=("Kode Hotel"), required=True)
    kode_ruangan = forms.CharField(label=("Kode Ruangan"), required=True, max_length=5)
    jenis_bed = forms.CharField(label=("Jenis Bed"), required=True, max_length=10)
    tipe = forms.CharField(label=("Tipe"), required=True, max_length=10)
    harga_per_hari = forms.IntegerField(label=("Harga per Hari"), required=True)

    def __init__(self, *args, **kwargs):
        super(Update_Ruangan_Hotel_Form, self).__init__(*args, **kwargs)
        self.fields['kode_hotel'].widget.attrs['readonly'] = True
        self.fields['kode_hotel'].widget.attrs['class'] = 'disabled'
        self.fields['kode_hotel'].widget.attrs['id'] = 'kode_hotel'
        self.fields['kode_ruangan'].widget.attrs['readonly'] = True
        self.fields['kode_ruangan'].widget.attrs['class'] = 'disabled'
        self.fields['kode_ruangan'].widget.attrs['id'] = 'kode_ruangan'
