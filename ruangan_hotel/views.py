from datetime import datetime
from django.db import connection
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from .forms import *

def user_login_required(function):
    def wrapper(request, *args, **kwargs):
        if 'is_login' not in request.session:
            return redirect('login:login_or_register')
        else:
            return function(request, *args, **kwargs)
    return wrapper

@ user_login_required
def create_ruangan_hotel(request):
    if request.session['peran'] != 'Admin Sistem':
        return redirect('login:login_or_register')
    print(request.session['username'])
    form = Create_Ruangan_Hotel_Form()
    if request.method == "POST":
        kode_hotel = request.POST['kode_hotel']
        kode_ruangan = request.POST['kode_ruangan']
        jenis_bed = request.POST['jenis_bed']
        tipe = request.POST['tipe']
        harga_per_hari = request.POST['harga_per_hari']
        data_baru = [kode_hotel, kode_ruangan, jenis_bed, tipe, harga_per_hari]
        print(data_baru)
        with connection.cursor() as cursor:
            cursor.execute(
                'INSERT INTO SIRUCO.HOTEL_ROOM VALUES (%s, %s, %s, %s, %s)', data_baru)
        return redirect('ruangan_hotel:read')
    return render(request, 'ruangan_hotel/create_ruangan_hotel.html', {'form': form, 'peran': request.session['peran']})

@csrf_exempt
def post_kode_ruangan(request):
    kode_hotel = request.GET.get('kode_hotel')
    print(kode_hotel)
    kode_ruangan = ''

    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT KODEROOM FROM SIRUCO.HOTEL_ROOM WHERE KODEHOTEL = %s ORDER BY KODEROOM DESC LIMIT 1;", [kode_hotel])
        data = cursor.fetchall()
        # print(data)
        if (data != []):
            databaru = data[0][0][2:]
            # print(databaru)
            kode_ruangan = "RH" + manip(databaru)
            print(kode_ruangan)
        else:
            kode_ruangan = "RH001"
    return JsonResponse({'kode_ruangan': kode_ruangan})


def manip(kode):
    res = ''
    nilai = int(kode)
    print(nilai)
    nilai += 1
    res += str(nilai).zfill(3)
    print(res, "WAWAUIHAW")
    return res

@user_login_required
def read_ruangan_hotel(request):
    if request.session['peran'] != 'Pengguna Publik' and request.session['peran'] != 'Admin Satgas' and request.session['peran'] != 'Admin Sistem':
        return redirect('login:login_or_register')
    else:
        tgl = datetime.today().strftime('%Y-%m-%d')
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.HOTEL_ROOM")
            dataquery = dictfetchall(cursor)
            cursor.execute("SELECT * FROM SIRUCO.HOTEL_ROOM AS H WHERE H.koderoom NOT IN (SELECT RH.koderoom FROM SIRUCO.RESERVASI_HOTEL AS RH WHERE H.kodehotel = RH.kodehotel AND RH.tglkeluar > %s)", [tgl])
            ruangankosong = dictfetchall(cursor)
            # cursor.execute("SELECT * FROM SIRUCO.HOTEL_ROOM AS H WHERE H.koderoom IN (SELECT RH.koderoom FROM SIRUCO.RESERVASI_HOTEL AS RH WHERE RH.tglkeluar < %s)", [tgl])
            # ruangankosong2 = dictfetchall(cursor)
            # print(ruangankosong2)
            context = {'data': dataquery, 'ruangankosong':ruangankosong}
    return render(request, 'ruangan_hotel/read_ruangan_hotel.html', context)

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

@ user_login_required
def update_ruangan_hotel(request):
    if request.session['peran'] != 'Admin Sistem':
        return redirect('login:login_or_register')
    form = Update_Ruangan_Hotel_Form()
    if request.method == 'GET':
        print(request.GET.get('kode_hotel'), request.GET.get('kode_ruangan'))
        if request.GET.get('kode_hotel') is not None and request.GET.get('kode_ruangan') is not None:
            kode_hotel = request.GET.get('kode_hotel')
            kode_ruangan = request.GET.get('kode_ruangan')
            with connection.cursor() as cursor:
                cursor.execute(
                    'SELECT * FROM SIRUCO.HOTEL_ROOM where kodehotel = %s AND koderoom = %s LIMIT 1;', [kode_hotel, kode_ruangan])
                data = dictfetchall(cursor)
                print("datanya")
                print(data)
            data_ruangan = {}
            data_ruangan['kode_hotel'] = data[0]['kodehotel']
            data_ruangan['kode_ruangan'] = data[0]['koderoom']
            data_ruangan['jenis_bed'] = data[0]['jenisbed']
            data_ruangan['tipe'] = data[0]['tipe']
            data_ruangan['harga_per_hari'] = data[0]['harga']
            form = Update_Ruangan_Hotel_Form(initial=data_ruangan)
    else:
        kode_hotel = request.POST['kode_hotel']
        kode_ruangan = request.POST['kode_ruangan']
        jenis_bed = request.POST['jenis_bed']
        tipe = request.POST['tipe']
        harga_per_hari = request.POST['harga_per_hari']
        data_to_update = [jenis_bed, tipe, harga_per_hari, kode_hotel, kode_ruangan]
        with connection.cursor() as cursor:
            cursor.execute(
                "UPDATE SIRUCO.HOTEL_ROOM SET jenisbed = %s, tipe = %s, harga = %s WHERE kodehotel = %s and koderoom = %s", data_to_update)
        return redirect('ruangan_hotel:read')
    return render(request, 'ruangan_hotel/update_ruangan_hotel.html', {'form': form})

@user_login_required
def delete_ruangan_hotel(request):
    if request.session['peran'] != 'Admin Sistem':
        return redirect('login:login_or_register')
    if request.method == 'GET':
        if request.GET.get('kode_hotel') is not None and request.GET.get('kode_ruangan') is not None:
            kode_hotel = request.GET.get('kode_hotel')
            kode_ruangan = request.GET.get('kode_ruangan')
            with connection.cursor() as cursor:
                cursor.execute(
                    'DELETE FROM SIRUCO.HOTEL_ROOM WHERE kodehotel = %s AND koderoom = %s', [kode_hotel, kode_ruangan])
    return HttpResponseRedirect(reverse('ruangan_hotel:read'))