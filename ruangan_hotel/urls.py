from django.urls import path
from .views import create_ruangan_hotel, post_kode_ruangan, read_ruangan_hotel,update_ruangan_hotel, delete_ruangan_hotel

app_name = 'ruangan_hotel'

urlpatterns = [
    path('create/', create_ruangan_hotel, name='create'),
    path('post/', post_kode_ruangan, name = "post_kode_ruangan"),
    path('read/', read_ruangan_hotel, name='read'),
    path('update/', update_ruangan_hotel, name='update'),
    path('delete/', delete_ruangan_hotel, name='delete'),
]