from django.urls import path

# from .views import login, logout_view, login_or_register, register, register_admin_sistem, register_pengguna_publik, register_dokter, register_admin_satgas
from .views import create_reservasi_rs, ajaxRuanganRS, ajaxBedRS, update_reservasi_rs, read_reservasiRS, delete_reservasi_rs
app_name = 'reservasiRS'

urlpatterns = [
    path('create/', create_reservasi_rs, name='create'),
    path('ajaxRuanganRS/', ajaxRuanganRS, name='ajaxRuanganRS'),
    path('ajaxBedRS/', ajaxBedRS, name='ajaxBedRS'),
    path('update/', update_reservasi_rs, name='update'),

    path('read/', read_reservasiRS, name='read'),
    # path('details/', detail_pasien, name='details'),
    path('delete/', delete_reservasi_rs, name='delete'),
]
