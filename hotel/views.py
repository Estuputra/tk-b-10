from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *
from django.db import connection

def user_login_required(function):
    def wrapper(request, *args, **kwargs):
        if 'is_login' not in request.session:
            return redirect('login:login_or_register')
        else:
            return function(request, *args, **kwargs)
    return wrapper

@user_login_required
def create_hotel(request):
    # Jika bukan admin sistem
    if request.session['peran'] != 'Admin Sistem':
        return redirect('login:login_or_register')
    print(request.session['username'])
    form = CreateHotelForm()
    if request.method == "POST":
        kode = request.POST['kode']
        nama = request.POST['nama']
        isRujukan = ''
        try:
            if request.POST['isRujukan'] == '1':
                isRujukan = '1'
            else:
                isRujukan = '0'
        except:
            isRujukan = '0'
        jalan = request.POST['jalan']
        kelurahan = request.POST['kelurahan']
        kecamatan = request.POST['kecamatan']
        kabkot = request.POST['kabkot']
        provinsi = request.POST['provinsi']
        data_baru = [kode, nama, isRujukan, jalan,
                     kelurahan, kecamatan, kabkot, provinsi]
        with connection.cursor() as cursor:
            cursor.execute(
                'INSERT INTO SIRUCO.HOTEL VALUES (%s, %s, %s, %s, %s, %s, %s, %s)', data_baru)
        return redirect('hotel:read')
    return render(request, 'CreateHotel.html', {'form': form, 'peran': request.session['peran']})

@user_login_required
def update_hotel(request):
    # Jika bukan admin sistem
    if request.session['peran'] != 'Admin Sistem':
        return redirect('login:login_or_register')
    if request.method == 'GET':
        if request.GET.get('kode') is not None:
            kode = request.GET.get('kode')
            with connection.cursor() as cursor:
                cursor.execute(
                    'SELECT * FROM SIRUCO.HOTEL where kode=%s LIMIT 1;', [kode])
                data = dictfetchall(cursor)
            data_hotel = {}
            data_hotel['kode'] = data[0]['kode']
            data_hotel['nama'] = data[0]['nama']
            isRujukan = data[0]['isrujukan']
            data_hotel['jalan'] = data[0]['jalan']
            data_hotel['kelurahan'] = data[0]['kelurahan']
            data_hotel['kecamatan'] = data[0]['kecamatan']
            data_hotel['kabkot'] = data[0]['kabkot']
            data_hotel['provinsi'] = data[0]['prov']
            form = UpdateHotelForm(initial=data_hotel)
    else:
        kode = request.POST['kode']
        nama = request.POST['nama']
        tipe = ''
        try:
            if request.POST['isRujukan'] == '1':
                tipe = '1'
            else:
                tipe = '0'
        except:
            tipe = '0'
        jalan = request.POST['jalan']
        kelurahan = request.POST['kelurahan']
        kecamatan = request.POST['kecamatan']
        kabkot = request.POST['kabkot']
        provinsi = request.POST['provinsi']
        new_data = [nama, tipe, jalan, kelurahan, kecamatan, kabkot, provinsi, kode]
        with connection.cursor() as cursor:
            cursor.execute(
                "UPDATE SIRUCO.HOTEL SET nama = %s, isrujukan = %s, jalan = %s,kelurahan = %s,kecamatan = %s,kabkot = %s,prov = %s WHERE KODE = %s", new_data)
        return redirect('hotel:read')
    return render(request, 'UpdateHotel.html', {'form': form})

@user_login_required
def read_hotel(request):
    if request.session['peran'] == 'Dokter':
        return redirect('login:login_or_register')
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.HOTEL")
        rekap = dictfetchall(cursor)
    context = {'apt': rekap}
    return render(request, 'ReadHotel.html', context)

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]