from django import forms
from django.db import connection


class CreateHotelForm(forms.Form):
    kode = forms.CharField(label=("Kode Hotel"), required=True, max_length=5)
    nama = forms.CharField(label=("Nama Hotel"), required=True, max_length=50)
    rujukan = forms.BooleanField(label=("Rujukan"), required=True)
    jalan = forms.CharField(label=("Jalan"), required=True, max_length=30)
    kelurahan = forms.CharField(label=("Kelurahan"), required=True, max_length=30)
    kecamatan = forms.CharField(label=("Kecamatan"), required=True, max_length=30)
    kabkot = forms.CharField(label=("Kabpuaten/Kota"), required=True, max_length=30)
    provinsi = forms.CharField(label=("Provinsi"), required=True, max_length=30)

    def __init__(self, *args, **kwargs):
        super(CreateHotelForm, self).__init__(*args, **kwargs)
        self.fields['kode'].widget.attrs['readonly'] = True
        self.fields['kode'].widget.attrs['class'] = 'disabled'
        kode_baru = ''
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT KODE FROM SIRUCO.HOTEL ORDER BY KODE DESC LIMIT 1")
            # print(cursor.fetchall()[0][0][1:])
            kode_baru = self.manip(cursor.fetchall()[0][0])
        self.fields['kode'].initial = kode_baru
        

    def manip(self, kode):
        res = ''
        nilai = int(kode)
        print(nilai)
        nilai += 1
        res += str(nilai)
        return res


class UpdateHotelForm(forms.Form):
    kode = forms.CharField(label=("Kode Hotel"), required=True, max_length=3)
    nama = forms.CharField(label=("Nama Hotel"), required=True, max_length=50)
    jalan = forms.CharField(label=("Jalan"), required=True, max_length=30)
    kelurahan = forms.CharField(label=("Kelurahan"), required=True, max_length=30)
    kecamatan = forms.CharField(label=("Kecamatan"), required=True, max_length=30)
    kabkot = forms.CharField(label=("Kabpuaten/Kota"),required=True, max_length=30)
    provinsi = forms.CharField(label=("Provinsi"), required=True, max_length=30)

    def __init__(self, *args, **kwargs):
        super(UpdateHotelForm, self).__init__(*args, **kwargs)
        self.fields['kode'].widget.attrs['readonly'] = True
        self.fields['kode'].widget.attrs['class'] = 'disabled'
