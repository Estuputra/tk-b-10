from django.urls import path
from .views import create_hotel, read_hotel, update_hotel

app_name = 'hotel'

urlpatterns = [
    path('create/', create_hotel, name='create'),
    path('read/', read_hotel, name='read'),
    path('update/', update_hotel, name='update'),
]