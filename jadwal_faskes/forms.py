from django import forms
from django.db import connection


class CreateJadwalFaskesForm(forms.Form):
    faskes_list = []
    faskes = forms.ChoiceField(choices=faskes_list, required=True)
    shift = forms.CharField(label=("Shift"), required=True, max_length=15)
    tanggal = forms.DateField(label=(
        "Tanggal"), required=True, widget=forms.DateInput(attrs={'type': 'date'}))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT KODE, NAMA FROM SIRUCO.FASKES")
            self.fields['faskes'].choices = cursor.fetchall()
