from django.shortcuts import render

# Create your views here.
from django.shortcuts import render

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *
from django.db import connection

#
# Create your views here.


def user_login_required(function):
    def wrapper(request, *args, **kwargs):
        if 'is_login' not in request.session:
            return redirect('login:login_or_register')
        else:
            return function(request, *args, **kwargs)
    return wrapper


@user_login_required
def create_jadwal_faskes(request):
    # Jika bukan pengguna publik
    if request.session['peran'] != 'Admin Satgas':
        return redirect('login:login_or_register')
    form = CreateJadwalFaskesForm()
    if request.method == "POST":
        kode_faskes = request.POST['faskes']
        shift = request.POST['shift']
        tanggal = request.POST['tanggal']
        data_baru = [kode_faskes, shift, tanggal]
        with connection.cursor() as cursor:
            cursor.execute(
                'INSERT INTO SIRUCO.JADWAL VALUES (%s, %s, %s)', data_baru)
        return redirect('login:login_or_register')
    return render(request, 'jadwal_faskes/createJadwalFaskes.html', {'form': form, 'peran': request.session['peran']})


@user_login_required
def read_jadwal_faskes(request):
    if request.session['peran'] != 'Admin Satgas':
        return redirect('login:login_or_register')
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.JADWAL")
        seluruh = dictfetchall(cursor)
    context = {'apt': seluruh}
    return render(request, 'jadwal_faskes/readJadwalFaskes.html', context)


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
