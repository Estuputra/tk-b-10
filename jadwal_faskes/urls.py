from django.urls import path

# from .views import login, logout_view, login_or_register, register, register_admin_sistem, register_pengguna_publik, register_dokter, register_admin_satgas
from .views import create_jadwal_faskes, read_jadwal_faskes
app_name = 'jadwal_faskes'

urlpatterns = [
    path('create/', create_jadwal_faskes, name='create'),
    path('read/', read_jadwal_faskes, name='read'),
]
