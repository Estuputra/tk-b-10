from django.urls import path


from .views import read_jadwal_dokter
app_name = 'jadwaldokter'


urlpatterns = [
    # path('create/', create_jadwal_dokter, name = 'create'),
    path('read/', read_jadwal_dokter, name='read'),
]
