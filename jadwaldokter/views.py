from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
# from .forms import *
from django.db import connection



def user_login_required(function):
    def wrapper(request, *args, **kwargs):
        if 'is_login' not in request.session:
            return redirect('login:login_or_register')
        else:
            return function(request, *args, **kwargs)
    return wrapper

# @user_login_required
# def create_jadwal_dokter(request):
#     if request.session['peran'] == 'Dokter':
#         return redirect('login:login_or_register')
        
#         with connection.cursor() as cursor:
#             cursor.execute(
#                 'INSERT INTO SIRUCO.JADWAL_DOKTER VALUES (%s, %s, %s)', data_baru)
#         return redirect('login:login_or_register')
#     return render(request, 'createjadwaldokter.html')



@user_login_required
def read_jadwal_dokter(request):
    if request.session['peran'] == 'Pengguna Publik' and request.session['peran'] == 'Admin Satgas':
        return redirect('login:login_or_register')
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.JADWAL_DOKTER")
        seluruh = dictfetchall(cursor)
    context = {'field': seluruh}
    return render(request, 'readjadwaldokter.html', context)

    
def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]