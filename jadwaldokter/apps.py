from django.apps import AppConfig


class JadwaldokterConfig(AppConfig):
    name = 'jadwaldokter'
