from django import forms
from django.db import connection


class LoginForm(forms.Form):
    username = forms.CharField(
        label=("Username"), required=True, max_length=50)
    password = forms.CharField(
        label=("Password"), widget=forms.PasswordInput, required=True, max_length=128)


class RegisterAdminSistemForm(forms.Form):
    email = forms.CharField(label=("Email"), required=True, max_length=50)
    password = forms.CharField(
        label=("Password"), widget=forms.PasswordInput, required=True, max_length=128)


class RegisterPenggunaPublikForm(forms.Form):
    email = forms.CharField(label=("Email"), required=True, max_length=50)
    password = forms.CharField(
        label=("Password"), widget=forms.PasswordInput, required=True, max_length=128)
    nama_lengkap = forms.CharField(
        label=("Nama Lengkap"), required=True, max_length=50)
    NIK = forms.CharField(label=("NIK"), required=True, max_length=20)
    no_hp = forms.CharField(label=("No HP"), required=True, max_length=12)


class RegisterDokterForm(forms.Form):
    email = forms.CharField(label=("Email"), required=True, max_length=50)
    password = forms.CharField(
        label=("Password"), widget=forms.PasswordInput, required=True, max_length=128)
    nama_lengkap = forms.CharField(
        label=("Nama Lengkap"), required=True, max_length=50)
    no_str = forms.CharField(label=("No STR"), required=True, max_length=20)
    no_hp = forms.CharField(label=("No HP"), required=True, max_length=20)
    gelar_depan = forms.CharField(
        label=("Gelar Depan"), required=True, max_length=20)
    gelar_belakang = forms.CharField(
        label=("Gelar Belakang"), required=True, max_length=20)


class RegisterAdminSatgasForm(forms.Form):
    email = forms.CharField(label=("Email"), required=True, max_length=50)
    password = forms.CharField(
        label=("Password"), widget=forms.PasswordInput, required=True, max_length=128)
    kode_faskes_list = []
    kode_faskes = forms.ChoiceField(choices=kode_faskes_list, required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT KODE, KODE FROM SIRUCO.FASKES")
            self.fields['kode_faskes'].choices = cursor.fetchall()


class UpdateProfileForm(forms.Form):
    username = forms.CharField(
        label=("Username"), required=True, max_length=30)
    password = forms.CharField(label=("Password"), widget=forms.PasswordInput(render_value=True),
                               required=True, max_length=30)
    peran = forms.CharField(
        label=("Peran"), required=True, max_length=30)

    def __init__(self, *args, **kwargs):
        super(UpdateProfileForm, self).__init__(*args, **kwargs)
        self.fields['peran'].disabled = True
        self.fields['password'].disabled = True
