from django.urls import path

from .views import profile, login, logout_view, login_or_register, register, register_admin_sistem, register_pengguna_publik, register_dokter, register_admin_satgas

app_name = 'login'

urlpatterns = [
    path('', login_or_register, name='login_or_register'),
    path('login/', login, name='login'),
    path('register/', register, name='register'),
    path('logout/', logout_view, name='logout'),
    path('register/adminsistem', register_admin_sistem,
         name='register_admin_sistem'),
    path('register/adminsatgas', register_admin_satgas,
         name='register_admin_satgas'),
    path('register/dokter', register_dokter, name='register_dokter'),
    path('register/penggunapublik', register_pengguna_publik,
         name='register_pengguna_publik'),



    path('profile/', profile, name='profile')


]
