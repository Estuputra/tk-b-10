from django.shortcuts import render, redirect
from django.forms import formset_factory
from django.db import connection

from .forms import LoginForm, RegisterDokterForm, RegisterAdminSatgasForm, RegisterAdminSistemForm, RegisterPenggunaPublikForm, UpdateProfileForm
# Create your views here.


def login(request):
    # Belum Login
    if 'is_login' not in request.session:
        form = LoginForm()
        if request.method == "POST":
            form = LoginForm(request.POST)
            if form.is_valid():
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']
                # check di tabel user
                with connection.cursor() as cursor:
                    cursor.execute(
                        f"SELECT * FROM SIRUCO.AKUN_PENGGUNA WHERE username='{username}' AND password='{password}'")
                    data = cursor.fetchone()

                print(data)
                # User Ketemu
                if data:
                    # cari tipe user
                    peran = None
                    request.session['username'] = data[0]
                    request.session['peran'] = data[2]
                    request.session['is_login'] = True

                    return redirect('login:login_or_register')
                # User Tidak Ketemu
                else:
                    return render(request, 'login/login.html', {'form': form, 'message': 'User tidak ditemukan!'})
        # Method Get (Halaman Login)
        return render(request, 'login/login.html', {'form': form})
    # Sudah Login
    else:
        return redirect('login:login_or_register')


def login_or_register(request):
    # Belum Login
    if 'is_login' not in request.session:
        return render(request, 'login/login_or_register.html')
    else:
        username = request.session['username']
        peran = request.session['peran']
        context = {'username': username, 'peran': peran}
        return render(request, 'login/landing_page.html', context)


def register(request):
    if 'is_login' in request.session:
        return redirect('login:login_or_register')
    return render(request, 'login/register.html')


def register_admin_sistem(request):
    if 'is_login' in request.session:
        return redirect('login:login_or_register')
    # Belum Login
    form = RegisterAdminSistemForm()
    if request.method == "POST":
        form = RegisterAdminSistemForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['email']
            password = form.cleaned_data['password']
            new_data = [username, password, 'Admin Sistem']
            # lolos trigger
            try:
                with connection.cursor() as cursor:
                    cursor.execute(
                        f"INSERT INTO SIRUCO.AKUN_PENGGUNA VALUES (%s, %s, %s)", new_data)
                    cursor.execute(
                        "INSERT INTO SIRUCO.ADMIN VALUES (%s)", [username])
                request.session['username'] = username
                request.session['peran'] = 'Admin Sistem'
                request.session['is_login'] = True
                return redirect('login:login_or_register')
            # galolos trigger
            except:
                print('galolos trigger')
                return render(request, 'login/register_admin_sistem.html', {'form': form, 'message': 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka!'})
    # Method Get (Halaman Login)
    return render(request, 'login/register_admin_sistem.html', {'form': form})


def register_pengguna_publik(request):
    if 'is_login' in request.session:
        return redirect('login:login_or_register')
    # Belum Login
    form = RegisterPenggunaPublikForm()
    if request.method == "POST":
        form = RegisterPenggunaPublikForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['email']
            password = form.cleaned_data['password']
            nama = form.cleaned_data['nama_lengkap']
            nik = form.cleaned_data['NIK']
            no_hp = form.cleaned_data['no_hp']
            new_data = [username, nik, nama, 'AKTIF', 'Pengguna Publik', no_hp]
            # lolos trigger
            try:
                with connection.cursor() as cursor:
                    cursor.execute(
                        f"INSERT INTO SIRUCO.AKUN_PENGGUNA VALUES (%s, %s, %s)", [username, password, 'Pengguna Publik'])
                    cursor.execute(
                        f"INSERT INTO SIRUCO.PENGGUNA_PUBLIK VALUES (%s, %s, %s, %s, %s, %s)", new_data)
                request.session['username'] = username
                request.session['peran'] = 'Pengguna Publik'
                request.session['is_login'] = True
                return redirect('login:login_or_register')

            # galolos trigger
            except:
                print('galolos trigger')
                return render(request, 'login/register_pengguna_publik.html', {'form': form, 'message': 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka!'})
    # Method Get (Halaman Login)
    return render(request, 'login/register_pengguna_publik.html', {'form': form})


def register_dokter(request):
    if 'is_login' in request.session:
        return redirect('login:login_or_register')
    form = RegisterDokterForm()
    if request.method == "POST":
        form = RegisterDokterForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['email']
            password = form.cleaned_data['password']
            nama = form.cleaned_data['nama_lengkap']
            no_str = form.cleaned_data['no_str']
            gelar_depan = form.cleaned_data['gelar_depan']
            gelar_belakang = form.cleaned_data['gelar_belakang']
            no_hp = form.cleaned_data['no_hp']
            new_data = [username, no_str, nama,
                        no_hp, gelar_depan, gelar_belakang]
            # lolos trigger
            try:
                with connection.cursor() as cursor:
                    cursor.execute(
                        f"INSERT INTO SIRUCO.AKUN_PENGGUNA VALUES (%s, %s, %s)", [username, password, 'Dokter'])
                    cursor.execute(
                        "INSERT INTO SIRUCO.ADMIN VALUES (%s)", [username])
                    cursor.execute(
                        f"INSERT INTO SIRUCO.DOKTER VALUES (%s, %s, %s, %s, %s, %s)", new_data)
                request.session['username'] = username
                request.session['peran'] = 'Dokter'
                request.session['is_login'] = True
                return redirect('login:login_or_register')

            # galolos trigger
            except:
                print('galolos trigger')
                return render(request, 'login/register_dokter.html', {'form': form, 'message': 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka!'})
    # Method Get (Halaman Login)
    return render(request, 'login/register_dokter.html', {'form': form})


def register_admin_satgas(request):
    if 'is_login' in request.session:
        return redirect('login:login_or_register')
    # Belum login
    form = RegisterAdminSatgasForm()
    if request.method == "POST":
        form = RegisterAdminSatgasForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['email']
            password = form.cleaned_data['password']
            new_data = [username, password, 'Admin Satgas']
            kode_faskes = form.cleaned_data['kode_faskes']
            # lolos trigger
            try:
                with connection.cursor() as cursor:
                    cursor.execute(
                        f"INSERT INTO SIRUCO.AKUN_PENGGUNA VALUES (%s, %s, %s)", new_data)
                    cursor.execute(
                        "INSERT INTO SIRUCO.ADMIN VALUES (%s)", [username])
                    cursor.execute(
                        "INSERT INTO SIRUCO.ADMIN_SATGAS VALUES (%s, %s)", [username, kode_faskes])

                request.session['username'] = username
                request.session['peran'] = 'Admin Satgas'
                request.session['is_login'] = True
                return redirect('login:login_or_register')
            # galolos trigger
            except:
                print('galolos trigger')
                return render(request, 'login/register_admin_satgas.html', {'form': form, 'message': 'Password Anda belum memenuhi syarat, silahkan pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka!'})
    # Method Get (Halaman Login)
    return render(request, 'login/register_admin_satgas.html', {'form': form})


def logout_view(request):
    del request.session['username']
    del request.session['peran']
    del request.session['is_login']
    return redirect('login:login')


def profile(request):
    if 'is_login' not in request.session:
        return redirect('login:login_or_register')
    if request.method == 'GET':
        username = request.session['username']
        with connection.cursor() as cursor:
            cursor.execute(
                'SELECT * FROM SIRUCO.AKUN_PENGGUNA where username=%s;', [username])
            data = dictfetchall(cursor)
        data_profil = {}
        print(data[0])
        data_profil['username'] = data[0]['username']
        data_profil['password'] = data[0]['password']
        data_profil['peran'] = data[0]['peran']
        form = UpdateProfileForm(initial=data_profil)
    else:
        # print('WOIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII')
        # print(request.POST)
        username = request.POST['username']
        with connection.cursor() as cursor:
            cursor.execute(
                    "select username from SIRUCO.AKUN_PENGGUNA")
            data = dictfetchall(cursor)
        for i in range(len(data)):
            if username == data[i]['username']:
                with connection.cursor() as cursor:
                    cursor.execute(
                        'SELECT * FROM SIRUCO.AKUN_PENGGUNA where username=%s;', [request.session['username']])
                    data2 = dictfetchall(cursor)
                data_profil = {}
                data_profil['username'] = data2[0]['username']
                data_profil['password'] = data2[0]['password']
                data_profil['peran'] = data2[0]['peran']
                form = UpdateProfileForm(initial=data_profil)
                return render(request, 'login/profile.html', {'form': form,'message':'username sudah ada!'})
        data_baru = [username, request.session['username']]
        print(data_baru, 111111111111111111111111111)
        peran = request.session['peran']
        request.session['username'] = username
        if peran == 'Admin Sistem':
            with connection.cursor() as cursor:
                cursor.execute(
                    "UPDATE SIRUCO.AKUN_PENGGUNA SET username = %s WHERE username = %s", data_baru)
                cursor.execute(
                    "UPDATE SIRUCO.ADMIN SET USERNAME = %s WHERE USERNAME = %s", data_baru
                )
        elif peran == 'Admin Satgas':
            with connection.cursor() as cursor:
                print(data_baru, 'OISS')
                cursor.execute(
                    "UPDATE SIRUCO.AKUN_PENGGUNA SET username = %s WHERE username = %s", data_baru)
                cursor.execute(
                    "UPDATE SIRUCO.ADMIN SET username = %s WHERE username = %s", data_baru)
                cursor.execute(
                    "UPDATE SIRUCO.ADMIN_SATGAS SET username = %s WHERE username = %s", data_baru)
        elif peran == 'Dokter':
            with connection.cursor() as cursor:
                cursor.execute(
                    "UPDATE SIRUCO.AKUN_PENGGUNA SET username = %s WHERE username = %s", data_baru)
                cursor.execute(
                    "UPDATE SIRUCO.ADMIN SET username = %s WHERE username = %s", data_baru)
                cursor.execute(
                    "UPDATE SIRUCO.DOKTER SET username = %s WHERE username = %s", data_baru)
        else:
            with connection.cursor() as cursor:
                cursor.execute(
                    "UPDATE SIRUCO.AKUN_PENGGUNA SET username = %s WHERE username = %s", data_baru)
                cursor.execute(
                    "UPDATE SIRUCO.PENGGUNA_PUBLIK SET username = %s WHERE username = %s", data_baru)
        return redirect('login:login_or_register')
    return render(request, 'login/profile.html', {'form': form})


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
