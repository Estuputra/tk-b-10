from django import forms
from django.db import connection

class Update_Transaksi_Hotel(forms.Form):
    id_transaksi = forms.CharField(
        label=("ID Transaksi"), required=True, max_length=3)
    kode_pasien = forms.CharField(
        label=("Kode Pasien"), required=True, max_length=30)
    tanggal_pembayaran = forms.DateField(label=(
        "Tanggal Pembayaran"), required=True, widget=forms.DateInput(attrs={'type': 'date'}))
    waktu_pembayaran = forms.DateField(label=(
        "Waktu Pembayaran"), required=True, widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}))
    total_bayar = forms.CharField(
        label=("Total Bayar"), required=True, max_length=30)
    status_bayar = forms.CharField(label=("Status Bayar"), required=True, max_length=30)

    def __init__(self, *args, **kwargs):
        super(Update_Transaksi_Hotel, self).__init__(*args, **kwargs)
        self.fields['id_transaksi'].widget.attrs['readonly'] = True
        self.fields['id_transaksi'].widget.attrs['class'] = 'disabled'
        self.fields['kode_pasien'].widget.attrs['readonly'] = True
        self.fields['kode_pasien'].widget.attrs['class'] = 'disabled'
        self.fields['tanggal_pembayaran'].widget.attrs['readonly'] = True
        self.fields['tanggal_pembayaran'].widget.attrs['class'] = 'disabled'
        self.fields['waktu_pembayaran'].widget.attrs['readonly'] = True
        self.fields['waktu_pembayaran'].widget.attrs['class'] = 'disabled'
        self.fields['total_bayar'].widget.attrs['readonly'] = True
        self.fields['total_bayar'].widget.attrs['class'] = 'disabled'
