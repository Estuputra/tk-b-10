from django.urls import path
from .views import read_transaksi_hotel, update_transaksi_hotel
app_name = 'transaksi_hotel'

urlpatterns = [
    path('read/', read_transaksi_hotel, name='read'),
    path('update/', update_transaksi_hotel, name='update'),
]