from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *
from django.db import connection

def user_login_required(function):
    def wrapper(request, *args, **kwargs):
        if 'is_login' not in request.session:
            return redirect('login:login_or_register')
        else:
            return function(request, *args, **kwargs)
    return wrapper

@user_login_required
def update_transaksi_hotel(request):
    if request.session['peran'] != 'Admin Satgas':
        return redirect('login:login_or_register')
    if request.method == 'GET':
        if request.GET.get('idtransaksi') is not None:
            idtransaksi = request.GET.get('idtransaksi')
            with connection.cursor() as cursor:
                cursor.execute(
                    'SELECT * FROM SIRUCO.TRANSAKSI_HOTEL where idtransaksi=%s LIMIT 1;', [idtransaksi])
                data = dictfetchall(cursor)
            data_pasien = {}
            data_pasien['id_transaksi'] = data[0]['idtransaksi']
            data_pasien['kode_pasien'] = data[0]['kodepasien']
            data_pasien['tanggal_pembayaran'] = data[0]['tanggalpembayaran']
            data_pasien['waktu_pembayaran'] = data[0]['waktupembayaran']
            data_pasien['total_bayar'] = data[0]['totalbayar']
            data_pasien['status_bayar'] = data[0]['statusbayar']
            form = Update_Transaksi_Hotel(initial=data_pasien)
    else:
        idtransaksi = request.POST['id_transaksi']
        statusbayar = request.POST['status_bayar']

        data_to_update = [statusbayar, idtransaksi]
        with connection.cursor() as cursor:
            cursor.execute(
                "UPDATE SIRUCO.TRANSAKSI_HOTEL SET statusbayar = %s WHERE idtransaksi = %s", data_to_update)
        return redirect('transaksi_hotel:read')
    return render(request, 'transaksi_hotel/update_transaksi_hotel.html', {'form': form})

@user_login_required
def read_transaksi_hotel(request):
    if request.session['peran'] != 'Admin Satgas':
        return redirect('login:login_or_register')
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.TRANSAKSI_HOTEL")
        data = dictfetchall(cursor)
    context = {'apt': data}
    return render(request, 'transaksi_hotel/read_transaksi_hotel.html', context)


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
