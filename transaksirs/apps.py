from django.apps import AppConfig


class TransaksiRsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'transaksirs'
