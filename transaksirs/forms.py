from django import forms
from django.db import connection


class UpdateTransaksiRSForm(forms.Form):
    idtransaksi = forms.CharField(
        label=("ID Transaksi"), required=True, max_length=3)
    kodepasien = forms.CharField(
        label=("Kode Pasien"), required=True, max_length=30)
    tanggal_masuk = forms.DateField(label=(
        "Tanggal Masuk"), required=True, widget=forms.DateInput(attrs={'type': 'date'}))
    tanggal_pembayaran = forms.DateField(label=(
        "Tanggal Pembayaran"), required=True, widget=forms.DateInput(attrs={'type': 'date'}))
    waktu_pembayaran = forms.DateField(label=(
        "Waktu Pembayaran"), required=True, widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}))
    total_biaya = forms.CharField(
        label=("Total Biaya"), required=True, max_length=30)
    status_bayar = forms.CharField(label=("Status Bayar"),
                                   required=True, max_length=30)

    def __init__(self, *args, **kwargs):
        super(UpdateTransaksiRSForm, self).__init__(*args, **kwargs)
        self.fields['idtransaksi'].widget.attrs['readonly'] = True
        self.fields['idtransaksi'].widget.attrs['class'] = 'disabled'
        self.fields['tanggal_masuk'].widget.attrs['readonly'] = True
        self.fields['tanggal_masuk'].widget.attrs['class'] = 'disabled'
        self.fields['kodepasien'].widget.attrs['readonly'] = True
        self.fields['kodepasien'].widget.attrs['class'] = 'disabled'
        self.fields['tanggal_pembayaran'].widget.attrs['readonly'] = True
        self.fields['tanggal_pembayaran'].widget.attrs['class'] = 'disabled'
        self.fields['waktu_pembayaran'].widget.attrs['readonly'] = True
        self.fields['waktu_pembayaran'].widget.attrs['class'] = 'disabled'
        self.fields['total_biaya'].widget.attrs['readonly'] = True
        self.fields['total_biaya'].widget.attrs['class'] = 'disabled'
