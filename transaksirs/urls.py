from django.urls import path
from .views import read_transaksi_rs, update_transaksi_rs, delete_transaksi_rs
app_name = 'transaksirs'

urlpatterns = [
    path('read/', read_transaksi_rs, name='read'),
    path('update/', update_transaksi_rs, name='update'),
    path('delete/', delete_transaksi_rs, name='delete'),
]
